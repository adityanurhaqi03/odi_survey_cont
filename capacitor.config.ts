import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'ODISurvey',
  webDir: 'build',
  bundledWebRuntime: false,
  plugins: {
    NFC: {
      ios: {
        "associatedDomains": ["applinks:example.com"]
      }
    },
  }
};

export default config;
