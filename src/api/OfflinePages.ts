//OflinePages.ts
import { sendApiCheckLists } from '../api/api';
// import { CheckList } from '../models/CheckList';
import { getStoredAnswers, setStoredAnswers } from '../models/Storage';

interface CheckList {
  id: {
    [key: string]: {
      [key: string]: string;
    };
  };
  question_ids: Question[];
  title: string;
}

interface Question {
  constr_mandatory: boolean, 
  id: number, 
  label_ids: {id: number, value: string}[],
  question: string, 
  type: "simple_choice" | "text_box" | "datetime" | "numerical_box" | "free_text" | "char_box"
  type_input: "barcode" | "gps" | "nfc" | "image" | "false"
}

interface Data {
  create_date: string;
}

interface Answer {
  [key: string]: string;
}
const surveyId = 5;

const data: Data = {
  'create_date': '2023-03-10 00:00:00',
};

// export const sendSurveyData = async (surveyId: number) => {
//   const storedAnswers = getStoredAnswers(); // Get stored answers from localStorage
//   let id: CheckList = {
//     id: '',
//     question_ids: [],
//     title: ''
//   }; // Initialize pages object
//   const answers: Answer = {}; // Initialize answers object
//   let checkLists: CheckList[] = []; // Initialize checkLists array

//   // Get checklists from API or offline JSON file
//   try {
//     const response = await sendApiCheckLists(storedAnswers);
//     checkLists = response.checkLists;
//   } catch (error) {
//     console.error(error);
//   }

//   // Loop through checkLists to populate pages object and answers object
//   checkLists.forEach(checklist => {
//     checklist.question_ids.forEach((question) => {
//       const { id } = question;
//       const answer = storedAnswers[id.toString()];
//       if (answer !== undefined) {
//         if (!id[checklist.id]) {
//           id[checklist.id] = {};
//         }
//         id[checklist.id][`${surveyId}_${checklist.id}_${id}`] = answer;
//         answers[`${surveyId}_${checklist.id}_${id}`] = answer;
//       }
//     });
//   });
  

  // Send pages and answers to API
//   try {
//     const response = await sendApiCheckLists(storedAnswers);
//     setStoredAnswers({}); // Clear stored answers on successful submit
//     return response;
//   } catch (error) {
//     console.error(error);
//     setStoredAnswers(answers); // Store answers on failed submit
//     throw error;
//   }
// };
