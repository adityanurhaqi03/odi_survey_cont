import { CheckList, Question } from '../models/CheckList';
import { FileTransfer, FileTransferObject } from '@awesome-cordova-plugins/file-transfer';
import { File, FileEntry } from '@awesome-cordova-plugins/file';
import axios from 'axios';
import { CapacitorHttp } from '@capacitor/core';

const baseUrl = 'https://odi.smart-leaders.net/driver';
const username = '22063';
const password = '1935';
const offlineApiUrl = './odi.json';

export async function authenticate() {
  try {
    const response = await axios.post(
      "https://odi.smart-leaders.net/driver/authenticate",
      { 
        username, 
        password 
      },
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        }
      }
    );
    const accessToken = response.data.data[0].token;
    return accessToken;
  } catch (error) {
    console.error(error);
    throw error;
  }
}


// Function to get checklists from API or fallback to offline JSON file
export async function postNfcData(payload: {
  checklistId: string;
  questionId: string;
  answer: string;
}) 
{
  try {
    const response = await axios.post('/api/nfcdata', payload);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
} 

export const fileDownload = async (url: string) => {
  const fileTransfer: FileTransferObject = FileTransfer.create();
  const filename = url.match(/=[a-zA-Z._\s0-9]+.pdf/g)![0].substring(1);
  let entry: FileEntry | null = null;
  
  try {
    entry = await fileTransfer.download(url, File.externalRootDirectory + '/Download/' + filename);
  } catch (err) {
    alert("Download failed" + JSON.stringify(err))
  }

  if (!entry) {
    return "";
  } else {
    return entry.toURL();
  }
}

//if got access token
export const getApiCheckLists = async (surveyId: number) => {
  const isOnline = navigator.onLine; // Check if internet connection is available
  if (isOnline) {
    // Use API if online
    const accessToken = await authenticate();
    const formData = new FormData();
    formData.append('survey_id', surveyId.toString());

    try {
      const response = await axios.post(`${baseUrl}/get_survey`, formData, {
        headers: {
          "x-access-tokens": accessToken,
          "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        "Access-Control-Allow-Credentials": "true",
          "content-type": "application/x-www-form-urlencoded",
        }
      });
      const responseData = response.data;
      const checkLists = responseData.data as CheckList[];

      return {
        checkLists,
      };
    } catch (error) {
      console.error(error);
      throw error;
    }

  } else {
    // Fallback to offline JSON file if offline
    const response = await fetch(offlineApiUrl);
    const responseData = await response.json();
    const checkLists = responseData.data as CheckList[];

    return {
      checkLists,
    };
  }
};

const currentDate = new Date();

const createDate = {
  "create_date": currentDate.toISOString().slice(0, 19).replace("T", " ")
}

// Function to send checklists to API or show an error message if offline
export const sendApiCheckLists = async (pages : object) => {
  const isOnline = navigator.onLine; // Check if internet connection is available

  if (isOnline) {
    const accessToken = await authenticate();
    const formData = new FormData();
    formData.append("survey_id", "5");
    formData.append("data", JSON.stringify(createDate));
    formData.append("pages", JSON.stringify(pages));

    try {
      const response = await axios.post(`${baseUrl}/create_survey2`, formData, {
        headers: {
          "x-access-tokens": accessToken,
          "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization",
        "Access-Control-Allow-Credentials": "true",
        "content-type": "application/x-www-form-urlencoded",
        },
      });

      const responseData = response.data;
      const checkLists = responseData.data as CheckList[];
      const status = responseData.status as string;
      const desc_msg = responseData.desc_msg as string;

      return {
        status: status,
        message: desc_msg,
        checkLists
      };
    } catch (error) {
      console.error(error);

      return {
        status: 'error',
        message: 'Failed to send the request',
        checkLists: []
      };
    }
  } else {
    // Fallback to offline JSON file if offline
    const response = await fetch(offlineApiUrl);
    const responseData = await response.json();
    console.log(responseData)
    const checkLists = responseData.data as CheckList[] ;
    const status = responseData.status as string;
    const desc_msg = responseData.desc_msg as string;
    return {
      status: status,
      message: desc_msg,
      checkLists
    };
  }
};