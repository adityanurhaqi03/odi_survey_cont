  //createSurvey.ts
  import { sendApiCheckLists } from './api';
  import { CheckList, Question } from '../models/CheckList';
  
  // export const sendLocalStorageToApi = async () => {
  //   const isOnline = navigator.onLine;
  
  //   if (isOnline) {
  //     const storedData = JSON.parse(localStorage.getItem('checkListAnswers') as string);
  
  //     const formattedData: Record<string, Record<string, { answer: string }>> = {};
  
  //     for (const [key, value] of Object.entries(storedData)) {
  //       const [checklistId, questionId] = key.split('_').slice(1) as [string, string];

  //       if (!formattedData[checklistId]) {
  //         formattedData[checklistId] = {};
  //       }
  
  //       formattedData[checklistId][`5_${checklistId}_${questionId}`] = {
  //         answer: value,
  //       };
  //     }
  
  //     const response = await sendApiCheckLists(formattedData);
  
  //     if (response) {
  //       localStorage.removeItem('checkListAnswers');
  //     }
  
  //     return response;
  //   }
  
  //   return null;
  // };
