//Storage.ts
const ANSWERS_STORAGE_KEY = "answers";

export const getStoredAnswers = () => {
  const storedAnswers = localStorage.getItem(ANSWERS_STORAGE_KEY);
  return storedAnswers ? JSON.parse(storedAnswers) : {};
};

export const setStoredAnswers = (answers: { [key: number]: any }) => {
  localStorage.setItem(ANSWERS_STORAGE_KEY, JSON.stringify(answers));
};

