//CheckList.ts
//interface
export interface CheckList {
  id: string,
  question_ids: Question[],
  title: string
}

export interface Question {
  constr_mandatory: boolean, 
  id: number, 
  label_ids: {id: number, value: string}[],
  question: string, 
  type: "simple_choice" | "text_box" | "datetime" | "numerical_box" | "free_text" | "char_box"
  type_input: "barcode" | "gps" | "nfc" | "image" | "false"
}

export const ANSWERS_STORAGE_KEY = "answers";

export const getStoredAnswers = () => {
  const storedAnswers = localStorage.getItem(ANSWERS_STORAGE_KEY);
  return storedAnswers ? JSON.parse(storedAnswers) : {};
};

export const setStoredAnswers = (answers: { [key: number]: any }, checkList: CheckList) => {
  const storedAnswers = getStoredAnswers();
  storedAnswers[checkList.id] = {} as { [key: string]: any };
  for (const questionId in answers) {
    const question = checkList.question_ids.find((q) => q.id === Number(questionId));
    if (question) {
      if (question.type_input === "image") {
        storedAnswers[checkList.id][`5_${checkList.id}_${questionId}`] = { answer: "", answer_image: answers[questionId] };
      } else {
        storedAnswers[checkList.id][`5_${checkList.id}_${questionId}`] = { answer: answers[questionId] };
      }
    }
  }
  localStorage.setItem(ANSWERS_STORAGE_KEY, JSON.stringify(storedAnswers));
};

