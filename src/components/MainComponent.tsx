import { IonButton, IonRouterLink } from '@ionic/react';
import { useHistory } from 'react-router';

const MainComponent: React.FC = () => {
  const history = useHistory();

  const handleSurveyClick = () => {
    history.push('/surveylist');
  };

  return (
    <div>
      <h1>Selamat Datang!</h1>
      <p>Klik tombol di bawah ini untuk memulai survey:</p>
      <IonButton onClick={handleSurveyClick} expand="block">
        Survey 8
      </IonButton>
      {/* <IonButton onClick={handleSurveyClick} expand="block">
        Survey 7
      </IonButton> */}
    </div>
  );
};

export default MainComponent;
