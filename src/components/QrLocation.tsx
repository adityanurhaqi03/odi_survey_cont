import React, { useState, useEffect } from "react";
import { CheckList, Question, getStoredAnswers, setStoredAnswers  } from "../models/CheckList";
import { getApiCheckLists, sendApiCheckLists  } from "../api/api";
import {
IonButton,IonGrid,IonItem,IonLabel,IonRouterLink,IonRow,IonText,IonSpinner,IonCol,IonCheckbox,IonContent,
} from "@ionic/react";
import { BarcodeScanner } from "@capacitor-community/barcode-scanner";
import { Geolocation, Position } from "@capacitor/geolocation";
import "./ExploreContainer.css";
import "./QrLocation.css";
import { useHistory } from "react-router";

interface ContainerProps {
setCameraOn: (value: boolean) => void;
cameraOn: boolean;
}

const QrLocation: React.FC<ContainerProps> = ({ setCameraOn, cameraOn }) => {
  const [checked, setChecked] = useState(false);
  const [checkList, setCheckList] = useState<CheckList | undefined>();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [answers, setAnswers] = useState<{ [key: string]: string | any }>(
    getStoredAnswers()
  );
  const CACHE_KEY = "api-checklists";
  const [question, setquestion] = useState<Question | any>();

  
  useEffect(() => {
    const fetchChecklists = async () => {
    setLoading(true);

      try {
        const cache = await caches.open("api-data");
        const cachedResponse = await cache.match(CACHE_KEY);

        if (cachedResponse) {
          const data = await cachedResponse.json();
          setCheckList(data.checkLists[0]);
        } else {
          
          const response = await getApiCheckLists(5);
          if (response && response.checkLists.length) {
            setCheckList(response.checkLists[0]);
            cache.put(CACHE_KEY, new Response(JSON.stringify(response)));
          }
        }
      
      } catch (error) {
        console.error(error);
      }

      setLoading(false);
    };

    fetchChecklists();
  }, []);

const handleClick = () => {
history.push("/surveylist");
};

const handleClick2 = () => {
history.push("/nfc");
};

const handleQRCodeScan = async (id: number) => {
try {
// Check camera permission
await BarcodeScanner.checkPermission({ force: true });
setCameraOn(true);
// Start scanning and wait for a result
const result = await BarcodeScanner.startScan();
  
        // If the result has content
  if (result.hasContent) {
    setAnswers((prevState) => {
      return {
        ...prevState,
        [id]: result.content,
      };
    });
  }

  // Stop scanning
  stopScan();
} catch (error) {
  console.error(error);
  // Show error message to user
  alert("Failed to scan QR code");

    }
  };
  
  const stopScan = async () => {
    setCameraOn(false);
    BarcodeScanner.showBackground();
    await BarcodeScanner.stopScan();
    const ionContent = document.querySelector('ion-content') as HTMLElement;
    if (ionContent) {
      ionContent.style.removeProperty('background');
    }
  };
  
const handleGPSLocation = async (id: number) => {
  try {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000, // timeout dalam milidetik
      maximumAge: 0, // data yang paling baru
    };
    const position: Position = await Geolocation.getCurrentPosition(options);;
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const response = await fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}`);
    const data = await response.json();
    const locationName = data.display_name;
    setAnswers({
      ...answers,
      [id]: `${locationName}`,
    });
  } catch (error) {
    console.error(error);
  }
};

const handleTextboxChange = (e: React.ChangeEvent<HTMLInputElement>, id: number) => {
  setAnswers({ ...answers, [id]: e.target.value });
};
useEffect(() => {
  if (checkList) {
    setStoredAnswers(answers, checkList);
  }
}, [answers, checkList]);



  return (
    <><IonRow style={{ display: "flex", flexDirection: "column", alignItems: "flex-end", marginTop:"20px" }}>
      {cameraOn ? (
        <IonButton onClick={stopScan}>Stop Scan</IonButton>
      ) : null }
    </IonRow>
    <div className='qrcontainer'>
        {loading ? (
          <IonSpinner />
        ) : checkList ? (
          <div className='content'>
            <IonRow style={{ marginTop: '30px' }}>
              {cameraOn ? null : (
                <IonText style={{ "--background": cameraOn ? "transparent" : "var(--ion-background-color, #fff)" }}>{checkList.title}</IonText>
              )}
            </IonRow>
            <IonItem style={{ "--background": cameraOn ? "transparent" : "var(--ion-background-color, #fff)" }} class='content'>
              <div className='input-wrapper'>
                {checkList.question_ids.map((question: Question) => (
                  <IonGrid class='gridqr' key={question.id}>
                    {cameraOn ? null : (
                      <IonLabel style={{ "--background": cameraOn ? "transparent" : "var(--ion-background-color, #fff)", display: 'block', marginBottom: '10px' }}>{question.question}</IonLabel>
                    )}
                    {question.type === 'char_box' && (
                      <>
                        {cameraOn ? null : (
                          <input
                          type="text"
                          style={{ backgroundColor: 'white', display: 'block' }}
                          onChange={(e) => {
                            handleTextboxChange(e, question.id);
                            localStorage.setItem(`${question.id}`, e.target.value);
                          }}
                          value={answers[question.id] || ''}
                        ></input>
                        )}
                      </>
                    )}
                    <IonRow style={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap', marginTop: '10px' }}>
                      {question.type === 'char_box' && question.type_input === 'barcode' && (
                        <>
                          {cameraOn ? null : (
                            <IonButton onClick={() => handleQRCodeScan(question.id)}>Scan QR Code</IonButton>
                          )}
                        </>
                      )}
                      {question.type === 'char_box' && question.type_input === 'gps' && (
                        <>
                          {cameraOn ? null : (
                            <IonButton onClick={() => handleGPSLocation(question.id)}>Get GPS Location</IonButton>
                          )}
                        </>
                      )}
                    </IonRow>
                  </IonGrid>
                ))}
              </div>
            </IonItem>

            {cameraOn ? null : (
              <IonGrid style={{ paddingBottom: '30px' }}>
                <IonCol>
                  <IonButton onClick={handleClick}>Back</IonButton>
                </IonCol>
                <IonCol>
                  <IonButton 
                  // disabled={!Object.values(answers).every((answer) => answer)} 
                  onClick={handleClick2}>
                    Next
                  </IonButton>
                </IonCol>
              </IonGrid>
            )}
          </div>
        ) : (
          <h1>No data found</h1>
        )}
      </div></>
  );
};

export default QrLocation;
