import React, { useEffect, useState } from 'react';
import { IonButton, IonGrid, IonRow, IonItem, IonCol, IonSpinner, IonText, IonLabel, useIonToast } from '@ionic/react';
import { CheckList, Question, getStoredAnswers, setStoredAnswers  } from '../models/CheckList';
import { authenticate, getApiCheckLists } from '../api/api';
import { useHistory } from 'react-router-dom';
import { NFC } from '@awesome-cordova-plugins/nfc';
import { Plugins } from '@capacitor/core';
import './QrLocation.css';

interface ContainerProps {}

const DateNfc: React.FC<ContainerProps> = () => {
  const history = useHistory();
  const [checkList, setCheckList] = useState<CheckList | undefined>();
  const [answers, setAnswers] = useState<{ [key: number]: string | undefined }>(
    getStoredAnswers()
    );
  const [loading, setLoading] = useState(false);
  const [present] = useIonToast();
  const CACHE_KEY = 'api-checklists';
  const { Haptics } = Plugins;
  const flags = NFC.FLAG_READER_NFC_A | NFC.FLAG_READER_NFC_V;
  const accessToken = localStorage.getItem("accessToken");

  useEffect(() => {
    const fetchChecklists = async () => {
    setLoading(true);

      try {
        const cache = await caches.open("api-data");
        const cachedResponse = await cache.match(CACHE_KEY);

        if (cachedResponse) {
          const data = await cachedResponse.json();
          setCheckList(data.checkLists[1]);
        } else {
          // const accessToken = await authenticate();
          const response = await getApiCheckLists(5); 
          if (response && response.checkLists.length) {
            setCheckList(response.checkLists[1]);
            cache.put(CACHE_KEY, new Response(JSON.stringify(response)));
          }
        }
      } catch (error) {
        console.error(error);
      }
      setLoading(false);
    };

    fetchChecklists();
  }, []);
  

  const scanNFC = async (id: number) => {
    const readerMode$ = NFC.readerMode(flags).subscribe(
      tag => {
        if (tag.id) {
          const nfc_id = NFC.bytesToHexString(tag.id);
          setAnswers({ ...answers, [id]: nfc_id });
          Haptics.vibrate();
        }
      },
      err => console.log('Error reading NFC', err),
      () => console.log('NFC scan completed')
    );
  
    // Stop scanning after 10 seconds
    setTimeout(() => {
      readerMode$.unsubscribe();
    }, 10000);
  };

  const presentToast = async (position: 'top') => {
    const isEnabled = await NFC.enabled();
    if (isEnabled) {
      present({
        message: 'NFC Sudah Aktif',
        duration: 1500,
        position: position,
        color: 'success'
      });
    } else {
      present({
        message: 'NFC Tidak Aktif',
        duration: 1500,
        position: position,
        color: 'danger'
      });
    }
  };
  
  const handleClick = () => {
    history.push('/qr');
  };

  const handleClick2 = () => {
    history.push('/check');
  };

  const handleTextboxChange = (e: React.ChangeEvent<HTMLInputElement>, id: number) => {
    setAnswers({ ...answers, [id]: e.target.value });
  };
  const handleDatetimeChange = (e: React.ChangeEvent<HTMLInputElement>, id: number) => {
    const date = new Date(e.target.value);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;
    setAnswers({ ...answers, [id]: formattedDate });
  };
  useEffect(() => {
    if (checkList) {
      setStoredAnswers(answers, checkList);
    }
  }, [answers, checkList]);
  
  return (
    <div className='qrcontainer'>
      {loading ? (
        <IonSpinner />
      ) : checkList ? (
        <div className='content'>
          <IonRow style={{ marginTop: '30px' }}>
            <IonText>{checkList.title}</IonText>
          </IonRow>
          <IonItem class='content'>
            <div className='input-wrapper'>
              {checkList.question_ids.map((question: Question) => (
                <IonGrid className='gridqr' key={question.id}>
                  <IonLabel style={{ display: 'block', marginBottom: '10px', marginTop: '30px' }}>
                    {question.question}
                  </IonLabel>
                  <IonRow>
                    {question.type === 'datetime' && (
                      <input
                        type='datetime-local'
                        style={{ backgroundColor: 'white' }}
                        onChange={(e) => handleDatetimeChange(e, question.id)}
                        value={answers[question.id] || ''}
                      ></input>
                    )}
                    <IonRow style={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap' }}>
                      {question.type === 'char_box' && (
                        <input
                          type='false'
                          style={{ backgroundColor: 'white' }}
                          onChange={(e) => handleTextboxChange(e, question.id)}
                          value={answers[question.id] || ''}
                        ></input>
                      )}
                       {question.type === 'char_box' && question.type_input === 'nfc' && (
                    <IonButton style={{ marginTop: '10px' }} onClick={() => {scanNFC(question.id); presentToast('top');}}>NFC</IonButton>
                  )}
                    </IonRow>
                  </IonRow>
                </IonGrid>
              ))}
            </div>
          </IonItem>
          <IonGrid style={{ paddingBottom: '30px' }}>
            <IonCol>
              <IonButton onClick={handleClick}>Back</IonButton>
            </IonCol>
            <IonCol>
              <IonButton
                // disabled={!Object.values(answers).every((answer) => answer)}
                onClick={handleClick2}
              >
                Next
              </IonButton>
            </IonCol>
          </IonGrid>
        </div>
      ) : (
        <h1>No data found</h1>
      )}
    </div>
  );
};

export default DateNfc;