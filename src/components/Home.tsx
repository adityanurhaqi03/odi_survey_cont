import { IonContent, IonHeader, IonIcon, IonImg, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Home.css';
import MainComponent from '../components/MainComponent';
import { Icon } from '@iconify/react';

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonContent class='container'>
      {/* <IonImg class="header" src="../assets/Pertamina_Logo.png" alt="pertamina"></IonImg> */}
      <Icon icon="ri:survey-fill" style={{ width: '250px', height: '100px' , margin: 'auto' , marginTop:'30px'}} />
      <MainComponent />
      </IonContent>
    </IonPage>
  );
};

export default Home;
