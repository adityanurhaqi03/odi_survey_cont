import { IonButton, IonGrid, IonRow, IonInput, IonItem, IonCheckbox, IonCol, IonText, IonList, IonRadioGroup, IonLabel, IonRadio, IonSpinner, InputChangeEventDetail, SegmentChangeEventDetail } from '@ionic/react';
import { useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { Plugins } from '@capacitor/core';
import { authenticate, getApiCheckLists ,sendApiCheckLists} from '../api/api';
import { CheckList, Question, getStoredAnswers, setStoredAnswers  } from '../models/CheckList';
import { ANSWERS_STORAGE_KEY } from '../models/CheckList';
import './QrLocation.css';
// import { sendSurveyData } from '../api/OfflinePages';

interface ContainerProps { }

const NumericalBox: React.FC<ContainerProps> = () => {
  const [checked, setChecked] = React.useState(false);
  const history = useHistory();
  const { Geolocation } = Plugins;
  const [selected, setSelected] = useState<{ [key: number]: string | undefined }>(getStoredAnswers());
  const [checkList, setCheckList] = useState<CheckList | any>();
  const [answers, setAnswers] = useState<{ [key: number]: string | undefined }>(
    getStoredAnswers()
  );
  const [showAlert, setShowAlert] = useState(false);
  const [loading, setLoading] = useState(false);
  const CACHE_KEY = 'api-checklists';
  const accessToken = localStorage.getItem("accessToken");

  useEffect(() => {
    const fetchChecklists = async () => {
      setLoading(true);
  
      try {
        const cache = await caches.open("api-data");
        const cachedResponse = await cache.match(CACHE_KEY);
  
        if (cachedResponse) {
          const data = await cachedResponse.json();
          setCheckList(data.checkLists[4]);
        } else {
          // const accessToken = await authenticate();
          const response = await getApiCheckLists(5);
          if (response && response.checkLists.length) {
            setCheckList(response.checkLists[4]);
            cache.put(CACHE_KEY, new Response(JSON.stringify(response)));
          }
        }
      } catch (error) {
        console.error(error);
      }
  
      setLoading(false);
    };
  
    fetchChecklists();
  }, []);
  
  const handleClick = () => {
    history.push('/radio');
  };
  
  const handleTextboxChange = (event: CustomEvent<InputChangeEventDetail>, questionId: number) => {
    const value = event.detail.value || '';
  
    setSelected({
      ...selected,
      [questionId]: value,
    });
  
    const newAnswers = {
      ...answers,
      [questionId]: value,
    };
  
    setAnswers(newAnswers);
  
    setStoredAnswers(newAnswers, checkList);
  };
  
const handleSubmit = async () => {
  setLoading(true);
  try {
    const response = await sendApiCheckLists(getStoredAnswers());
    // localStorage.removeItem(ANSWERS_STORAGE_KEY);
    setShowAlert(true);
    const storedAnswersTxt = JSON.stringify(getStoredAnswers(), null, 2);
    const txtFile = new Blob([storedAnswersTxt], {type: 'text/plain'});
    const txtUrl = URL.createObjectURL(txtFile);
    const a = document.createElement("a");
    a.href = txtUrl;
    a.download = "stored-answers.txt";
    a.click();
    // Menampilkan isi setStoredAnswers di layar
    alert("Isi setStoredAnswers telah ditampilkan pada file 'stored-answers.txt'");
    console.log(response.checkLists); // Melihat data yang diterima dari server di console
  } catch (error) {
    console.error(error);
    alert("Gagal!"); // Menampilkan pesan gagal di layar
  }
  setLoading(false);
};

        return (
          <div className='qrcontainer'>
            {loading ? (
        <IonSpinner />
      ) : checkList ? (
        <div className='content'>
          <IonRow style={{ marginTop: '30px' }}>
          <IonText style={{ fontSize: '20px', fontWeight: '700', margin: 'auto', marginBottom:"30px"}}>{checkList.title}</IonText>
          </IonRow>
          <IonItem class="content">
          <div className='input-wrapper'>
          {checkList.question_ids.map((question: Question, index: number) => (
  <IonGrid style={{ border: "1px solid gray", borderRadius: "10px", borderWidth: "2px", width: "100%" }} class='gridqr' key={question.id}>
    <IonLabel style={{ display: 'block', margin:"auto", marginBottom: "20px", paddingTop:"10px", maxWidth: "300px" }}>
      {`${index + 1}. ${question.question}`}
    </IonLabel> 
    {question.type === 'simple_choice' ? (
 <div style={{ display:"flex", flexDirection: "column", alignContent: "flex-start", alignItems:"center", margin:"auto"}}>
 <IonList style={{ margin:"auto" }}>
   <IonRadioGroup value={selected[question.id]} onIonChange={(e: CustomEvent<SegmentChangeEventDetail>) => {
     const newAnswers = {
       ...selected,
       [question.id]: e.detail.value
     };
     setSelected(newAnswers);
     if (checkList) {
      setStoredAnswers(newAnswers, checkList);}
   }}>
     {question.label_ids.map((label, index) => (
       <IonItem key={index}>
         <IonLabel>{label.value}</IonLabel>
         <IonRadio name={`${question.id}`} value={label.id.toString()} />
       </IonItem>
     ))}
   </IonRadioGroup>
 </IonList>
      </div>
    ) : question.type === 'numerical_box' ? (
      <div style={{ display:"flex", flexDirection:"column" }}>
         <IonLabel position="floating" style={{ marginLeft:"10px" }}>Write Here</IonLabel>
         <IonInput
  type='number'
  style={{ border: "1px solid gray", borderRadius: "10px", width:"100%" }}
  name={`${question.id}`}
  onIonChange={(event: CustomEvent<InputChangeEventDetail>) => handleTextboxChange(event, question.id)}
  value={answers[question.id] ?? ""}
></IonInput>
      </div>
    ) : question.type === 'text_box' ? (
      <div>
        <IonLabel position="floating" style={{ marginLeft:"10px" }}>Write Here</IonLabel>
        <IonInput
  type='text'
  style={{ border: "1px solid gray", borderRadius: "10px", width:"100%" }}
  name={`${question.id}`}
  onIonChange={(event: CustomEvent<InputChangeEventDetail>) => handleTextboxChange(event, question.id)}
  value={answers[question.id] ?? ""}
></IonInput>
      </div>
    ) : (null)}
  </IonGrid>
))}
              </div>
              </IonItem>
              <IonGrid style={{ paddingBottom: '30px' }}>
                <IonCol>
                  <IonButton onClick={handleClick}>Back</IonButton>
                </IonCol>
                <IonCol>
                <IonButton onClick={handleSubmit} disabled={loading}>
    {loading ? 'Loading...' : 'Submit'}
  </IonButton>
                </IonCol>
              </IonGrid>
            </div>
         ) : (
          <h1>No data found</h1>
          )}
      </div>
      );
    };
export default NumericalBox;
