import { IonButton } from '@ionic/react';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import { getStoredAnswers, setStoredAnswers } from '../models/Storage';

interface ContainerProps { }

const SurveyList: React.FC<ContainerProps> = () => {
  const [draftAnswers, setDraftAnswers] = useState<{ [key: string]: string }>({});
  const [draftList, setDraftList] = useState<{ [key: string]: string | undefined }>({});
  const history = useHistory();

  // useEffect(() => {
  //   const storedAnswers = getStoredAnswers();
  //   setDraftList(storedAnswers);
  // }, []);

  const handleSurveyClick = () => {
    history.push('/qr');
  };

  const isDraftIncomplete = () => {
    return (
      !draftAnswers['QrLocation'] ||
      !draftAnswers['DateNfc'] ||
      !draftAnswers['CheckboxType'] ||
      !draftAnswers['Checkbox2'] ||
      !draftAnswers['NumericalBox']
    );
  };

  const formatDraftDate = () => {
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hour = date.getHours();
    const minute = date.getMinutes();
    const second = date.getSeconds();
    return `${day}/${month}/${year} ${hour}:${minute}:${second}`;
  };

  const handleDraftAnswerChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setDraftAnswers({ ...draftAnswers, [name]: value });
  };

  const handleSaveDraftClick = () => {
    const timestamp = new Date().getTime();
    setStoredAnswers({ ...draftList, [timestamp]: draftAnswers });
    setDraftAnswers({});
    alert('Draft saved!');
  };
  

  return (
    <div className="container">
      <h1>Survey List</h1>
      {isDraftIncomplete() && (
        <p>Your draft is incomplete. Please fill in all the questions.</p>
      )}
      <p>These are your draft answers:</p>
      <ul>
        {Object.entries(draftList).map(([surveyId, answers]) => (
          <li key={surveyId}>
            <strong>Survey ID:</strong> {surveyId}
            <ul>
              {answers &&
                Object.entries(answers).map(([question, answer]) => (
                  <li key={question}>
                    <strong>{question}:</strong> {answer}
                  </li>
                ))}
            </ul>
          </li>
        ))}
        {isDraftIncomplete() && (
          <li>
            <strong>Created at:</strong> {formatDraftDate()}
            <ul>
              {Object.entries(draftAnswers).map(([question, answer]) => (
                <li key={question}>
                  <strong>{question}:</strong> <input type="text" name={question} value={answer} onChange={handleDraftAnswerChange} />
                </li>
              ))}
            </ul>
            <IonButton onClick={handleSaveDraftClick} disabled={isDraftIncomplete()}>
              Save Draft
            </IonButton>
          </li>
        )}
      </ul>
      <IonButton onClick={handleSurveyClick} expand="block">
        Start Survey
      </IonButton>
    </div>
  );
};

export default SurveyList;
