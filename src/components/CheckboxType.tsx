import { IonButton, IonGrid, IonRow, IonInput, IonItem, IonCheckbox, IonCol, IonText, IonList, IonRadioGroup, IonLabel, IonRadio, IonSpinner, SegmentChangeEventDetail } from '@ionic/react';
import { useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { getApiCheckLists, sendApiCheckLists} from '../api/api';
import { CheckList, Question, getStoredAnswers, setStoredAnswers } from '../models/CheckList';
import { Camera, CameraResultType } from '@capacitor/camera';

interface ContainerProps { }

const CheckboxType: React.FC<ContainerProps> = () => {
  const [checked, setChecked] = React.useState(false);
  const history = useHistory();
  const [selected, setSelected] = useState<{ [key: string]: string | any }>(getStoredAnswers());

  const [checkList, setCheckList] = useState<CheckList | any>();
  const [answers, setAnswers] = useState<{ [key: string]: any }>(getStoredAnswers());
  const [loading, setLoading] = useState(false);
  const CACHE_KEY = 'api-checklists';
  const accessToken = localStorage.getItem("accessToken");

  useEffect(() => {
    const fetchChecklists = async () => {
      setLoading(true);

      try {
        const cache = await caches.open("api-data");
        const cachedResponse = await cache.match(CACHE_KEY);

        if (cachedResponse) {
          const data = await cachedResponse.json();
          setCheckList(data.checkLists[2]);
        } else {
          // if (accessToken) {
          const response = await getApiCheckLists(5);
          if (response && response.checkLists.length) {
            setCheckList(response.checkLists[2]);
            cache.put(CACHE_KEY, new Response(JSON.stringify(response)));
          }
          }
        // }
      } catch (error) {
        console.error(error);
      }
      setLoading(false);
    };

    fetchChecklists();
  }, []);

  const takePicture = async (name: string) => {
    const picture = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
    });

    const date = new Date();
    const timestamp = date.getTime();
    const filename = `image_${timestamp}.jpeg`;
    const base64StringWithPrefix = "b'" + (picture as any).base64String! + "'";
    const newEntry = {
      file: base64StringWithPrefix,
      filename: filename
    };

    const newAnswers = { ...answers, [name]: newEntry };
    setAnswers(newAnswers);
    if (checkList) {
      setStoredAnswers(newAnswers, checkList);
    }
  };

    const handleClick = () => {
    history.push('/nfc');
    };

    const handleClick2 = () => {
        history.push('/radio');
        };
  
        return (
          <div className='qrcontainer'>
            {loading ? (
        <IonSpinner />
      ) : checkList ? (
        <div className='content'>
          <IonRow style={{ marginTop: '30px' }}>
          <IonText style={{ fontSize: '20px', fontWeight: '700', margin: 'auto', marginBottom:"30px"}}>{checkList.title}</IonText>
          </IonRow>
          <IonItem class="content">
          <div className='input-wrapper'>
          {checkList.question_ids.map((question: Question, index: number) => (
  <IonGrid style={{ border: "1px solid gray", borderRadius: "10px", borderWidth: "2px", width: "100%" }} class='gridqr' key={question.id}>
    <IonLabel style={{ display: 'block', margin:"auto", marginBottom: "20px", paddingTop:"10px"}}>
      {`${index + 1}. ${question.question}`}
    </IonLabel> 
    {question.type === 'simple_choice' ? (
  <div style={{ display:"flex", flexDirection: "column", alignContent: "flex-start", alignItems:"center", margin:"auto"}}>
   <IonList style={{ margin:"auto" }}>
    <IonRadioGroup value={selected[question.id]} onIonChange={(e: CustomEvent<SegmentChangeEventDetail>) => {

      const newAnswers = {
        ...selected,
        [question.id]: e.detail.value,
        [`${question.id}_image`]: answers[`${question.id}`], // Menambahkan nilai image ke objek jawaban (answers)
      };
      setSelected(newAnswers);
if (checkList) {
  setStoredAnswers(newAnswers, checkList);
}
    }}>
      {question.label_ids.map((label, index) => (
        <IonItem key={index}>
          <IonLabel>{label.value}</IonLabel>
          <IonRadio name={`${question.id}`} value={label.id.toString()} />
        </IonItem>
      ))}
    </IonRadioGroup>
  </IonList>
  {question.type_input === 'image' && (
  <div style={{ display:"flex", flexDirection:"column" }}>
    <IonLabel style={{ display: 'block', margin: '10px' }}>{answers[`${question.id}`]?.filename}</IonLabel>
    <IonButton style={{ margin:"auto" }} type="button" onClick={() => takePicture(`${question.id}`)}>Take Picture</IonButton>
  </div>
)}
  </div>
    ) : null}
  </IonGrid>
))}
   </div>
  </IonItem>
      <IonGrid style={{ paddingBottom: '30px' }}>
                <IonCol>
                  <IonButton onClick={handleClick}>Back</IonButton>
                </IonCol>
                <IonCol>
                <IonButton onClick={handleClick2}>Next</IonButton>
                </IonCol>
              </IonGrid>
            </div>
         ) : (
          <h1>No data found</h1>
          )}
      </div>
      );
    };
export default CheckboxType;
