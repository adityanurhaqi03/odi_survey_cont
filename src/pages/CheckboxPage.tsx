import { IonContent, IonImg, IonPage, IonRow, IonTitle } from '@ionic/react';
import { Icon } from '@iconify/react';
import './Survey.css'
import CheckboxType from '../components/CheckboxType';

const CheckboxPage: React.FC = () => {
  return (
    <IonPage>
      <IonContent class='container'>
      {/* <IonImg class="header" src="https://docs-demo.ionic.io/assets/madison.jpg" alt="pertamina"></IonImg>
      <Icon icon="ri:survey-fill" style={{ width: '160px', height: '90px' , margin: 'auto' , marginTop:'30px', marginBottom: ''}} /> */}
      <IonContent>
        <CheckboxType />
        </IonContent>
      </IonContent>
    </IonPage>
  );
};

export default CheckboxPage;
