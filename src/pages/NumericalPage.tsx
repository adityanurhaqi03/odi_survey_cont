import { IonContent, IonImg, IonPage, IonRow, IonTitle } from '@ionic/react';
import { Icon } from '@iconify/react';

import NumericalBox from '../components/NumericalBox';

const NumericalPage: React.FC = () => {
  return (
    <IonPage>
      <IonContent class='container'>
      {/* <Icon icon="ri:survey-fill" style={{ width: '160px', height: '90px' , margin: 'auto' , marginTop:'30px', marginBottom: '30px'}} /> */}
      <IonContent>
        <NumericalBox />
        </IonContent>
      </IonContent>
    </IonPage>
  );
};

export default NumericalPage;