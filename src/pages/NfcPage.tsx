import { IonContent, IonHeader, IonImg, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { Icon } from '@iconify/react';
import './Survey.css'
import DateNfc from '../components/DateNfc';

const Survey: React.FC = () => {
  return (
    <IonPage>
      <IonContent class='container'>
      {/* <IonImg class="header" src="https://docs-demo.ionic.io/assets/madison.jpg" alt="pertamina"></IonImg> */}
      <Icon icon="ri:survey-fill" style={{ width: '120px', height: '70px' , margin: 'auto' , marginTop:'30px', marginBottom: '30px'}} />
        <DateNfc />
      </IonContent>
    </IonPage>
  );
};

export default Survey;
