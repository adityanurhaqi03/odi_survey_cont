import { IonContent, IonHeader, IonIcon, IonImg, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Home.css';
import { Icon } from '@iconify/react';
import SurveyList from '../components/SurveyList';

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonContent class='container'>
      {/* <IonImg class="header" src="../assets/Pertamina_Logo.png" alt="pertamina"></IonImg> */}
      <Icon icon="ri:survey-fill" style={{ width: '250px', height: '100px' , margin: 'auto' , marginTop:'30px'}} />
      <SurveyList />
      </IonContent>
    </IonPage>
  );
};

export default Home;
