import { IonContent, IonHeader, IonImg, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { Icon } from '@iconify/react';
import './Survey.css'
import QrLocation from '../components/QrLocation';
import { useState } from 'react';

const Survey: React.FC = () => {
  const [ cameraOn, setCameraOn ] = useState(false);
  return (
    <IonPage>
      <IonContent style={{ "--background": cameraOn ? "transparent": "var(--ion-background-color, #fff)" }} class='container'>
      {/* <IonImg class="header" src="https://docs-demo.ionic.io/assets/madison.jpg" alt="pertamina"></IonImg> */}
      {cameraOn ? null : (
      <Icon icon="ri:survey-fill" style={{ width: '120px', height: '70px' , margin: 'auto' , marginTop:'30px', marginBottom: '30px'}} />
      )} 
        <QrLocation setCameraOn={setCameraOn} cameraOn={cameraOn}  />
      </IonContent>
    </IonPage>
  );
};

export default Survey;
